module gitlab.com/IvanTurgenev/anabet

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.2.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/text v0.3.2
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/ini.v1 v1.57.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
	upper.io/db.v3 v3.7.1+incompatible
)
