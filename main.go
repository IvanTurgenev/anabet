package main

import (
	"fmt"

	"log"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	// "golang.org/x/text/cases"

	// "time"
	"bytes"
	"os"

	"strconv"
	"strings"

	"gopkg.in/ini.v1"
	// "reflect"
	"context"
	// "upper.io/db.v3"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/sqlite"
)

const xlsname = "data.xlsx"
const sheetname = "Sheet1"
const maindbname = "test.db"
const maintable = "table"
const resultsdbname = "results.db"
const resultablename = "result"

var xlscont = [][]string{}
var xlsheaders = []string{}
var configsportrow = sportRowrange{}
var filteredsportrows = [][]sportRow{}
var fillistlist [][]filtrange
var resultsnum = []int{}

func generateRange(list []int) []int {
	var start int = 0
	var end int = 0
	var templist []int
	for _, num := range list {
		if num > 0 && start == 0 {
			start = num
		} else if start > 0 && end == 0 {
			end = num
		}
		if start > 0 && end > 0 {
			templist = append(templist, makeRange(start, end)...)
			start = 0
			end = 0
		} else if start > 0 && num == 0 && end == 0 {
			templist = append(templist, start)
			start = 0
		}
	}
	return templist
}

func makeRange(min, max int) []int {
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}

type filtrange struct {
	ColType string
	Min     float64
	Max     float64
}

type sportRowrange struct {
	League              string
	BetfairHomePrice    []float64
	BetfairDrawPrice    []float64
	BetfairAwayPrice    []float64
	FTSHomePrice        []float64
	FTSDrawPrice        []float64
	FTSAwayPrice        []float64
	HomeDifference      []float64
	DrawDifference      []float64
	AwayDifference      []float64
	HomeColourIndex     []float64
	AwayColourIndex     []float64
	HomevDrawDifference []float64
	HomeVAwayDifference []float64
	AwayVDrawDifference []float64
}

type resultsumm struct {
	ResultName string  `db:"ResultName"`
	HomeBack   float64 `db:"HomeBack"`
	DrawBack   float64 `db:"DrawBack"`
	AwayBack   float64 `db:"AwayBack"`
	HomeLay    float64 `db:"HomeLay"`
	DrawLay    float64 `db:"DrawLay"`
	AwayLay    float64 `db:"AwayLay"`
}

type sportRow struct {
	Season              string  `db:"Season"`
	Date                string  `db:"Date"`
	Time                string  `db:"Time"`
	League              string  `db:"League"`
	HomeTeam            string  `db:"HomeTeam"`
	AwayTeam            string  `db:"AwayTeam"`
	BetfairHomePrice    float64 `db:"BetfairHomePrice"`
	BetfairDrawPrice    float64 `db:"BetfairDrawPrice"`
	BetfairAwayPrice    float64 `db:"BetfairAwayPrice"`
	FTSHomePrice        float64 `db:"FTSHomePrice"`
	FTSDrawPrice        float64 `db:"FTSDrawPrice"`
	FTSAwayPrice        float64 `db:"FTSAwayPrice"`
	HomeDifference      float64 `db:"HomeDifference"`
	DrawDifference      float64 `db:"DrawDifference"`
	AwayDifference      float64 `db:"AwayDifference"`
	FT                  string  `db:"FT"`
	FT1X2               string  `db:"FT1X2"`
	HomeBack            float64 `db:"HomeBack"`
	DrawBack            float64 `db:"DrawBack"`
	AwayBack            float64 `db:"AwayBack"`
	HomeLay             float64 `db:"HomeLay"`
	DrawLay             float64 `db:"DrawLay"`
	AwayLay             float64 `db:"AwayLay"`
	HGP                 float64 `db:"HGP"`
	HW                  float64 `db:"HW"`
	HD                  float64 `db:"HD"`
	HL                  float64 `db:"HL"`
	HPTS                float64 `db:"HPTS"`
	HAVG                float64 `db:"HAVG"`
	AGP                 float64 `db:"AGP"`
	AW                  float64 `db:"AW"`
	AD                  float64 `db:"AD"`
	AL                  float64 `db:"AL"`
	APTS                float64 `db:"APTS"`
	AAVG                float64 `db:"AAVG"`
	HomeColourIndex     float64 `db:"HomeColourIndex"`
	AwayColourIndex     float64 `db:"AwayColourIndex"`
	HomeTrueOdds        float64 `db:"HomeTrueOdds"`
	DrawTrueOdds        float64 `db:"DrawTrueOdds"`
	AwayTrueOdds        float64 `db:"AwayTrueOdds"`
	ImpliedChanceHome   float64 `db:"ImpliedChanceHome"`
	ImpliedChanceDraw   float64 `db:"ImpliedChanceDraw"`
	ImpliedChanceAway   float64 `db:"ImpliedChanceAway"`
	HomevDrawDifference float64 `db:"HomevDrawDifference"`
	HomeVAwayDifference float64 `db:"HomeVAwayDifference"`
	AwayVDrawDifference float64 `db:"AwayVDrawDifference"`
}

func sporttable(collname string) string {
	return "CREATE TABLE `" + collname + "` (`Idk` INTEGER NOT NULL PRIMARY KEY, `Season`,`Date`,`Time`," +
		"`League`,`HomeTeam`,`AwayTeam`,`BetfairHomePrice` REAL,`BetfairDrawPrice` REAL,`BetfairAwayPrice` REAL,`FTSHomePrice` REAL," +
		"`FTSDrawPrice` REAL,`FTSAwayPrice` REAL,`HomeDifference` REAL,`DrawDifference` REAL,`AwayDifference` REAL,`FT`,`FT1X2`,`HomeBack` REAL," +
		"`DrawBack` REAL,`AwayBack` REAL,`HomeLay` REAL,`DrawLay` REAL,`AwayLay` REAL,`HGP` REAL,`HW` REAL,`HD` REAL,`HL` REAL,`HPTS` REAL,`HAVG` REAL,`AGP` REAL,`AW` REAL,`AD` REAL," +
		"`AL` REAL,`APTS` REAL,`AAVG` REAL,`HomeColourIndex` INTEGER,`AwayColourIndex` INTEGER,`HomeTrueOdds` REAL,`DrawTrueOdds` REAL,`AwayTrueOdds` REAL," +
		"`ImpliedChanceHome` REAL,`ImpliedChanceDraw` REAL,`ImpliedChanceAway` REAL,`HomevDrawDifference` REAL,`HomeVAwayDifference` REAL,`AwayVDrawDifference`)"

}

func summtable(collname string) string {
	return "CREATE TABLE `" + collname + "` (`Idk` INTEGER NOT NULL PRIMARY KEY, `ResultName`,`HomeBack` REAL," +
		"`DrawBack` REAL,`AwayBack` REAL,`HomeLay` REAL,`DrawLay` REAL,`AwayLay` REAL)"

}

func createdb(dbname string) {
	_, err := os.Stat(dbname)
	if os.IsNotExist(err) {

		fmt.Println("Creating.. database")
		os.Create(dbname)
	} else {
		fmt.Println("Database already exists: " + dbname)
	}

}
func createtabledb(sess sqlbuilder.Database, tablename string, tabletype string) {
	switch tabletype {
	case "sport":
		_, err := sess.Exec(sporttable(tablename))
		if err != nil {
			log.Fatal(err)
		}
	case "summ":
		_, err := sess.Exec(summtable(tablename))
		if err != nil {
			log.Fatal(err)
		}
	default:
		fmt.Println("No table type found: " + tabletype)
		os.Exit(1)

	}

}

func openDB(dbname string) sqlbuilder.Database {
	var tempsettings = sqlite.ConnectionURL{
		Database: dbname, // Path to database file.
	}
	sess, err := sqlite.Open(tempsettings)
	if err != nil {
		log.Fatalf("db.Open(): %q\n", err)
	}

	return sess
}

func permuteStrings(parts ...[]string) (ret []string) {
	{
		var n = 1
		for _, ar := range parts {
			n *= len(ar)
		}
		ret = make([]string, 0, n)
	}
	var at = make([]int, len(parts))
	var buf bytes.Buffer
loop:
	for {
		// increment position counters
		for i := len(parts) - 1; i >= 0; i-- {
			if at[i] > 0 && at[i] >= len(parts[i]) {
				if i == 0 || (i == 1 && at[i-1] == len(parts[0])-1) {
					break loop
				}
				at[i] = 0
				at[i-1]++
			}
		}
		// construct permutated string
		buf.Reset()
		for i, ar := range parts {
			var p = at[i]
			if p >= 0 && p < len(ar) {
				buf.WriteString(ar[p])
			}
		}
		ret = append(ret, buf.String())
		at[len(parts)-1]++
	}
	return ret
}

func permuteFilt(parts ...[]filtrange) (ret [][]filtrange) {
	{
		var n = 1
		for _, ar := range parts {
			n *= len(ar)
		}
		ret = make([][]filtrange, 0, n)
	}
	var at = make([]int, len(parts))
	var buf []filtrange
loop:
	for {
		// increment position counters
		for i := len(parts) - 1; i >= 0; i-- {
			if at[i] > 0 && at[i] >= len(parts[i]) {
				if i == 0 || (i == 1 && at[i-1] == len(parts[0])-1) {
					break loop
				}
				at[i] = 0
				at[i-1]++
			}
		}
		// construct permutated string
		buf = []filtrange{}
		for i, ar := range parts {
			var p = at[i]
			if p >= 0 && p < len(ar) {
				buf = append(buf, ar[p])
			}
		}
		ret = append(ret, buf)
		at[len(parts)-1]++
	}
	return ret
}

func cinum(s string) float64 {
	switch s {
	case "Grey":
		return 0
	case "Red":
		return 1
	case "Amber":
		return 2
	case "Green":
		return 3
	}
	fmt.Println("Error, bad data")
	os.Exit(1)
	return -1
}

func ftx(s string) int {
	switch s {
	case "1":
		return 1
	case "2":
		return 2
	case "X":
		return 3
	}
	fmt.Println("Error, bad data: " + s)
	return -1

}

func tofloat(s string) float64 {
	if s == "" {
		fmt.Println("Empty cell")
		return -1
	}
	if s == "NA" {
		return -1
	}
	f, err := strconv.ParseFloat(s, 32)
	if err != nil {
		fmt.Println("Error parsing float: " + s)
		return -1
	}
	return f

}

func popuDB(sess sqlbuilder.Database, rows [][]string) {
	ctx := context.Background()
	tx, _ := sess.NewTx(ctx)
	for _, row := range rows {
		_, err := tx.Collection(maintable).Insert(sportRow{
			Season:              row[0],
			Date:                row[1],
			Time:                row[2],
			League:              row[3],
			HomeTeam:            row[4],
			AwayTeam:            row[5],
			BetfairHomePrice:    tofloat(row[6]),
			BetfairDrawPrice:    tofloat(row[7]),
			BetfairAwayPrice:    tofloat(row[8]),
			FTSHomePrice:        tofloat(row[9]),
			FTSDrawPrice:        tofloat(row[10]),
			FTSAwayPrice:        tofloat(row[11]),
			HomeDifference:      tofloat(row[12]),
			DrawDifference:      tofloat(row[13]),
			AwayDifference:      tofloat(row[14]),
			FT:                  row[15],
			FT1X2:               row[16],
			HomeBack:            tofloat(row[17]),
			DrawBack:            tofloat(row[18]),
			AwayBack:            tofloat(row[19]),
			HomeLay:             tofloat(row[20]),
			DrawLay:             tofloat(row[21]),
			AwayLay:             tofloat(row[22]),
			HGP:                 tofloat(row[23]),
			HW:                  tofloat(row[24]),
			HD:                  tofloat(row[25]),
			HL:                  tofloat(row[26]),
			HPTS:                tofloat(row[27]),
			HAVG:                tofloat(row[28]),
			AGP:                 tofloat(row[29]),
			AW:                  tofloat(row[30]),
			AD:                  tofloat(row[31]),
			AL:                  tofloat(row[32]),
			APTS:                tofloat(row[33]),
			AAVG:                tofloat(row[34]),
			HomeColourIndex:     cinum(row[35]),
			AwayColourIndex:     cinum(row[36]),
			HomeTrueOdds:        tofloat(row[37]),
			DrawTrueOdds:        tofloat(row[38]),
			AwayTrueOdds:        tofloat(row[39]),
			ImpliedChanceHome:   tofloat(row[40]),
			ImpliedChanceDraw:   tofloat(row[41]),
			ImpliedChanceAway:   tofloat(row[42]),
			HomevDrawDifference: tofloat(row[43]),
			HomeVAwayDifference: tofloat(row[44]),
			AwayVDrawDifference: tofloat(row[45]),
		})
		if err != nil {
			fmt.Println(err)

		}
	}
	err := tx.Commit()
	if err != nil {
		fmt.Println(err)
	}

}
func removeIndex(s []string, index int) []string {
	return append(s[:index], s[index+1:]...)
}
func existin(slice []int, val int) bool {
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}

func delnumber(s string) string {
	_, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return s
	}
	return ""
}

func openXL() {
	f, err := excelize.OpenFile(xlsname)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	xlscont, err = f.GetRows(sheetname)

	for i, s := range xlscont[0] {
		s = strings.TrimSpace(s)
		xlscont[0][i] = strings.ReplaceAll(s, " ", "")
	}
	xlsheaders = xlscont[0]

	xlscont = xlscont[1:]
}

func createdfiltlist(listfilt []float64, nm string) []filtrange {
	var templist []filtrange
	var taken int = 0
	for i := range listfilt {
		if taken == 0 {
			tempfiltrange := filtrange{
				ColType: nm,
				Min:     listfilt[i],
				Max:     listfilt[1+i],
			}
			taken++
			templist = append(templist, tempfiltrange)
		} else {
			taken = 0

		}

	}
	return templist

}

func alltrue(bs []bool) bool {
	if bs == nil {
		return false
	}
	allTrue := true
	for _, b := range bs {
		if !b {
			allTrue = false
			break
		}
	}
	return allTrue

}

func filtrow(fr []filtrange, row sportRow) bool {
	var tempboollist []bool
	for _, filt := range fr {
		// fmt.Println(filt)
		switch filt.ColType {
		case "BetfairHomePrice":
			if row.BetfairHomePrice >= filt.Min && row.BetfairHomePrice <= filt.Max {
				tempboollist = append(tempboollist, true)

			} else {
				return false
			}
		case "BetfairDrawPrice":
			if row.BetfairDrawPrice >= filt.Min && row.BetfairDrawPrice <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "BetfairAwayPrice":
			if row.BetfairAwayPrice >= filt.Min && row.BetfairAwayPrice <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "FTSHomePrice":
			if row.FTSHomePrice >= filt.Min && row.FTSHomePrice <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "FTSDrawPrice":
			if row.FTSDrawPrice >= filt.Min && row.FTSDrawPrice <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "FTSAwayPrice":
			if row.FTSAwayPrice >= filt.Min && row.FTSAwayPrice <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "HomeDifference":
			if row.HomeDifference >= filt.Min && row.HomeDifference <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "DrawDifference":
			if row.DrawDifference >= filt.Min && row.DrawDifference <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "AwayDifference":
			if row.AwayDifference >= filt.Min && row.AwayDifference <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "HomeColourIndex":
			if row.HomeColourIndex >= filt.Min && row.HomeColourIndex <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "AwayColourIndex":
			if row.AwayColourIndex >= filt.Min && row.AwayColourIndex <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "HomevDrawDifference":
			if row.HomevDrawDifference >= filt.Min && row.HomevDrawDifference <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "HomeVAwayDifference":
			if row.HomeVAwayDifference >= filt.Min && row.HomeVAwayDifference <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		case "AwayVDrawDifference":
			if row.AwayVDrawDifference >= filt.Min && row.AwayVDrawDifference <= filt.Max {
				tempboollist = append(tempboollist, true)
			} else {
				return false
			}
		}
	}
	return alltrue(tempboollist)

}
func getfiltresuslts(rows []sportRow) [][]sportRow {
	var tempresults [][]sportRow
	tempfillistlist := permuteFilt(fillistlist...)
	for i, tempfiltlist := range tempfillistlist {
		fmt.Print("Combination " + strconv.Itoa(i) + ": ")
		fmt.Println(tempfiltlist)
		var temprows []sportRow
		for _, row := range rows {
			if filtrow(tempfiltlist, row) {
				temprows = append(temprows, row)
			}
		}
		tempresults = append(tempresults, temprows)
	}
	return tempresults
}

// func filtr(row sportRow, filts filtrange) []sportrow {
// 	var temprow sportRow
// 	if filts ==

// }

func loadconf() {
	cfgini, err := ini.Load("config.ini")
	if err != nil {
		fmt.Printf("Failed reading config file: %v", err)
		os.Exit(1)
	}
	configsportrow.BetfairHomePrice = cfgini.Section("").Key("BetfairHomePrice").Float64s(",")
	configsportrow.BetfairDrawPrice = cfgini.Section("").Key("BetfairDrawPrice").Float64s(",")
	configsportrow.BetfairAwayPrice = cfgini.Section("").Key("BetfairAwayPrice").Float64s(",")
	configsportrow.FTSHomePrice = cfgini.Section("").Key("FTSHomePrice").Float64s(",")
	configsportrow.FTSDrawPrice = cfgini.Section("").Key("FTSDrawPrice").Float64s(",")
	configsportrow.FTSAwayPrice = cfgini.Section("").Key("FTSAwayPrice").Float64s(",")
	configsportrow.HomeDifference = cfgini.Section("").Key("HomeDifference").Float64s(",")
	configsportrow.DrawDifference = cfgini.Section("").Key("DrawDifference").Float64s(",")
	configsportrow.AwayDifference = cfgini.Section("").Key("AwayDifference").Float64s(",")
	configsportrow.HomeColourIndex = cfgini.Section("").Key("HomeColourIndex").Float64s(",")
	configsportrow.AwayColourIndex = cfgini.Section("").Key("AwayColourIndex").Float64s(",")
	configsportrow.HomevDrawDifference = cfgini.Section("").Key("HomevDrawDifference").Float64s(",")
	configsportrow.HomeVAwayDifference = cfgini.Section("").Key("HomeVAwayDifference").Float64s(",")
	configsportrow.AwayVDrawDifference = cfgini.Section("").Key("AwayVDrawDifference").Float64s(",")
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.BetfairHomePrice, "BetfairHomePrice"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.BetfairDrawPrice, "BetfairDrawPrice"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.BetfairAwayPrice, "BetfairAwayPrice"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.FTSHomePrice, "FTSHomePrice"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.FTSDrawPrice, "FTSDrawPrice"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.FTSAwayPrice, "FTSAwayPrice"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.HomeDifference, "HomeDifference"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.DrawDifference, "DrawDifference"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.AwayDifference, "AwayDifference"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.HomeColourIndex, "HomeColourIndex"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.AwayColourIndex, "AwayColourIndex"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.HomevDrawDifference, "HomevDrawDifference"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.HomeVAwayDifference, "HomeVAwayDifference"))
	fillistlist = append(fillistlist, createdfiltlist(configsportrow.AwayVDrawDifference, "AwayVDrawDifference"))

}

func creaDefConf() {
	cfg := ini.Empty()
	cfg.Section("").Key("BetfairHomePrice").Comment = `# BetfairHomePrice filter`
	cfg.Section("").NewKey("BetfairHomePrice", "1.5,2.0,3.0,4.0")
	cfg.Section("").Key("BetfairDrawPrice").Comment = `# BetfairDrawPrice filter`
	cfg.Section("").NewKey("BetfairDrawPrice", "1.5,2.0,3.0,4.0")
	cfg.Section("").Key("BetfairAwayPrice").Comment = `# BetfairAwayPrice filter`
	cfg.Section("").NewKey("BetfairAwayPrice", "1.5,2.0,3.0,4.0")
	cfg.Section("").Key("FTSHomePrice").Comment = `# FTSHomePrice filter`
	cfg.Section("").NewKey("FTSHomePrice", "1.5,2.0,3.0,4.0")
	cfg.Section("").Key("FTSDrawPrice").Comment = `# FTSDrawPrice filter`
	cfg.Section("").NewKey("FTSDrawPrice", "1.5,2.0,3.0,4.0")
	cfg.Section("").Key("FTSAwayPrice").Comment = `# FTSAwayPrice filter`
	cfg.Section("").NewKey("FTSAwayPrice", "1.5,2.0,3.0,4.0")
	cfg.Section("").Key("HomeDifference").Comment = `# HomeDifference filter`
	cfg.Section("").NewKey("HomeDifference", "1.5,2.0,3.0,4.0")
	cfg.Section("").Key("DrawDifference").Comment = `# DrawDifference filter`
	cfg.Section("").NewKey("DrawDifference", "1.5,2.0,3.0,4.0")
	cfg.Section("").Key("AwayDifference").Comment = `# AwayDifference filter`
	cfg.Section("").NewKey("AwayDifference", "1.5,2.0,3.0,4.0")
	cfg.Section("").Key("HomeColourIndex").Comment = `# HomeColourIndex filter`
	cfg.Section("").NewKey("HomeColourIndex", "1.0,2.0,3.0,4.0")
	cfg.Section("").Key("AwayColourIndex").Comment = `# AwayColourIndex filter`
	cfg.Section("").NewKey("AwayColourIndex", "1.0,2.0,3.0,4.0")
	cfg.Section("").Key("HomevDrawDifference").Comment = `# HomevDrawDifference filter`
	cfg.Section("").NewKey("HomevDrawDifference", "1.0,2.0,3.0,4.0")
	cfg.Section("").Key("HomeVAwayDifference").Comment = `# HomeVAwayDifference filter`
	cfg.Section("").NewKey("HomeVAwayDifference", "1.0,2.0,3.0,4.0")
	cfg.Section("").Key("AwayVDrawDifference").Comment = `# AwayVDrawDifference filter`
	cfg.Section("").NewKey("AwayVDrawDifference", "1.0,2.0,3.0,4.0")

	var ext int8 = 0
	if confileExists("config.ini") {

	} else {
		fmt.Println("Creating config file")
		err1 := cfg.SaveTo("config.ini")
		if err1 != nil {
			fmt.Println(err1)
		}
		ext = 1
	}
	if ext == 1 {
		fmt.Println("Exiting edit new config file, and start again")
		os.Exit(1)
	}

}
func confileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// func cleandb() {
// 	for i := 0; true; i++ {
// 		sess, coll := openDB(resulttable + strconv.Itoa(i))
// 		defer sess.Close()
// 		err := coll.Truncate() // DELETES ALL DATA FROM TABLE
// 		if err != nil {
// 			log.Println("Truncate(): %q", err)
// 			break
// 		}
// 	}

// }

func insermutipledbresults(rowss [][]sportRow) {
	os.Remove(resultsdbname)
	fmt.Println("Deleting previous results if any..")
	createdb(resultsdbname)
	sess := openDB(resultsdbname)
	defer sess.Close() // Remember to close the database session.
	for i, rows := range rowss {
		resultsnum = append(resultsnum, i)
		var temptablename = resultablename + strconv.Itoa(i)
		createtabledb(sess, temptablename, "sport")
		ctx := context.Background()
		tx, _ := sess.NewTx(ctx)
		for _, row := range rows {
			_, err := tx.Collection(temptablename).Insert(row)
			if err != nil {
				fmt.Println("Error inserting result rows")
				fmt.Println(err)

			}

		}
		err := tx.Commit()
		if err != nil {
			fmt.Println(err)

		}
	}

}
func summresults(rowss [][]sportRow) []resultsumm {
	fmt.Println("Summarizing.")
	var tempresultsumlist []resultsumm
	for i, rows := range rowss {
		var tempresultsum resultsumm
		for _, row := range rows {
			tempresultsum.AwayBack = tempresultsum.AwayBack + row.AwayBack
			tempresultsum.AwayLay = tempresultsum.AwayLay + row.AwayLay
			tempresultsum.DrawBack = tempresultsum.DrawBack + row.DrawBack
			tempresultsum.DrawLay = tempresultsum.DrawLay + row.DrawLay
			tempresultsum.HomeBack = tempresultsum.HomeBack + row.HomeBack
			tempresultsum.HomeLay = tempresultsum.HomeLay + row.HomeLay

		}
		tempresultsum.ResultName = resultablename + strconv.Itoa(i)
		tempresultsumlist = append(tempresultsumlist, tempresultsum)

	}
	return tempresultsumlist

}

func insersummresults(rowss [][]sportRow) {
	sess := openDB(resultsdbname)
	defer sess.Close()
	createtabledb(sess, "ResultSumm", "summ")
	ctx := context.Background()
	tx, _ := sess.NewTx(ctx)
	resultlist := summresults(rowss)
	for _, result := range resultlist {
		_, err := tx.Collection("ResultSumm").Insert(result)
		if err != nil {
			fmt.Println("Error inserting results summary rows")
			fmt.Println(err)

		}

	}
	err := tx.Commit()
	if err != nil {
		fmt.Println(err)

	}

}

func main() {

	// fmt.Println(third)

	// var lls = [][]string{{"11", "22", "33"},
	// 	{},
	// 	{"AA", "BB", "CC"},
	// 	{"XX", "YY"}}
	// var ayo = permuteStrings(lls...)
	// fmt.Println(ayo)
	var argsWithoutProg []string

	argsWithoutProg = os.Args[1:]
	// argsWithoutProg = append(argsWithoutProg, "filt")
	if len(argsWithoutProg) == 0 {
		fmt.Println("Pass db or filt")

		os.Exit(1)
	}
	if argsWithoutProg[0] == "db" {
		createdb(maindbname)
		openXL()
		sess := openDB(maindbname)
		createtabledb(sess, maintable, "sport")
		defer sess.Close() // Remember to close the database session.
		// err := coll.Truncate() // DELETES ALL DATA FROM TABLE
		// if err != nil {
		// 	log.Fatalf("Truncate(): %q\n", err)
		// }
		popuDB(sess, xlscont)
	} else if argsWithoutProg[0] == "filt" {
		creaDefConf()
		loadconf()
		sess := openDB(maindbname)
		defer sess.Close()
		ctx := context.Background()
		tx, _ := sess.NewTx(ctx)
		res := tx.Collection(maintable).Find()
		var rows0 []sportRow

		err := res.All(&rows0)
		if err != nil {
			log.Fatalf("res.All(): %q\n", err)
		}
		// var filtered []sportRow
		filteredsportrows = getfiltresuslts(rows0)
		// finfilt(rows)
		insermutipledbresults(filteredsportrows)
		insersummresults(filteredsportrows)

		// for _, row := range filteredsportrows {
		// 	fmt.Println(row)
		// }

	}

}
